FROM python:3.12.0-slim-bookworm
WORKDIR /flaskapp
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["python","-m","flask","run","--host=0.0.0.0"]