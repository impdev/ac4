
# TemperoDaFe - Projeto Flask com Docker e serviços AWS

Este é um projeto chamado TemperoDaFe, um web site construido em Flask utilizando container Docker, alocado na AWS (EC2). Este projeto tem finalidade de aprender sobre comandos docker em configurações local. Aplicando testes unitários, testes de segurança e conceitos CI/CD por meio do GitLab Runner + Instância AWS. 


## Pré-requisitos

Certifique-se de que você tenha os seguintes pré-requisitos instalados em sua máquina para testes. (local):

- Docker: [Instalação do Docker](https://docs.docker.com/get-docker/)
- Wsl: [Instalação do WSL](https://learn.microsoft.com/pt-br/windows/wsl/install)


## Configuração

Faça um Fork do projeto [Tempero da Fe](https://gitlab.com/impdev/ac4) e clone para um repositório local


## Executando o aplicativo com Docker 

Execute o seguinte comando para construir a imagem e container do aplicativo usando Docker:

```bash
docker build -t ac4 .
docker run -dp 5000:5000 ac4
```

Para parar o container, você pode usar o seguinte comando, use "ls" para pegar o Id do container,
depois utilize o "stop" para parar o docker:

```bash
docker container ls
docker container stop <IdContainer>
```


## Página WEB

Acesse o aplicativo em [http://localhost:5000](http://localhost:5000) no seu navegador. Você pode se registrar, fazer login.


## AWS EC2 Instância + GitLab Runner

Configuração do GitLab runner usando uma instância EC2 (AWS), para execução dos testes unitários e testes de vulnerabilidade de código (SAST).

### Criar e conectar instância EC2
Acesse e siga a Etapa 1 para criar uma instancia [EC2 Linux](https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/EC2_GetStarted.html), guarde o par de chaves para conexão.

Conecte-se a instância por meio do [puTTY](https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/putty.html):
- Session: Host name -> Ip público da instância EC2
- Session>SSH>Auth: Credentials -> Selecionar o par de chaves na máquina local
- No terminal, logar como: "ubuntu"

### Configurações e requisitos

No terminal puTTY:

Instalação e Login Docker, após commando "login" informar usuário e senha
```bash
sudo apt update
sudo apt install docker.io
sudo docker login
```

Configuração GitLab-Runner, consiste em baixar e instalar o pacote de instalação debian do gitlab-runner usando a arquitetura (arch) amd64
```bash
curl -LJO "https://s3.amazonaws.com/gitlab-runner-downloads/v15.10.1/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
```

Registro do Runner, sequência de parâmentros para registro de um novo runner
```bash
sudo gitlab-runner register
```
Inserir na sequência:

- URL: "https://gitlab.com/"
- Token: Acesse no GitLab o grupo do projeto -> Click na aba lateral Build>Runners -> 3 pontos (superior direita) copie e cole o Registration token
- Descrição do Runner
- Tags (Opcional)
- Notas de Manutenção (Opicional)
- Executor: "docker"
- Imagem: "python"


### Testes e artefatos

No GitLab acesse o projeto -> Click na aba lateral build>Pipelines. Caso não tenha iniciado de form automática, click em Run Pipeline ->Run Pipeline.

Na coluna stages ao clickar no marcador é possível observar os estágios ou jobs, acessando-os visulize o terminal com as instruções executadas. 

Ao lado de stages há um ícone de download onde é diponibilizado os artefatos da pipeline. Como por exemplo neste projeto a fins de aprendizado há duas vulnerabilidades:

- Debug Mode: o teste SAST identificou que a atual versão do projeto possui o debug mode ativado no arquivo "app.py"
- Hard Code:  no arquivo JSON informa vulnerabilidade no arquivo "app.py" a qual possui uma secret_key sem uma variável segura e sim uma string binária explícita


## Contribuição

Integrantes:


      Nome: Hygor Rodrigues Cesconetto
      RA : 2203138
 
      Nome : Pedro Luis Da Paz Dos Santos
      RA: 2202636

      Nome Ewerton Ferreira Costa
      RA : 2202829

      Nome : Nicole Santos Matos
      RA : 2202409
    
      Nome : João Victor Machado Maniezzo
      RA : 2202323